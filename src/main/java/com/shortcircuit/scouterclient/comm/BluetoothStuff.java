package com.shortcircuit.scouterclient.comm;

import com.shortcircuit.nbn.Nugget;
import com.shortcircuit.nbn.nugget.NuggetSerializable;
import com.shortcircuit.scouterclient.ScouterClient;
import com.shortcircuit.scouterclient.sql.SQLQuery;

import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;
import java.io.DataInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * @author ShortCircuit908
 *         Created on 1/14/2016
 */
public class BluetoothStuff implements Runnable {
	private final String URL = "btspp://localhost:7674047e6e474bf0831f209e3f9dd23f;name=ScouterClient;authenticate=true;encrypt=true";
	private final StreamConnectionNotifier notifier;

	public BluetoothStuff() throws IOException {
		notifier = (StreamConnectionNotifier) Connector.open(URL);
		new Thread(this).start();
	}

	@Override
	public void run() {
		while (true) {
			try {
				stuff();
			}
			catch (IOException e) {
				break;
			}
		}
		closeSilent();
	}

	public void closeSilent() {
		try {
			close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void close() throws IOException {
		notifier.close();
	}

	@SuppressWarnings("unchecked")
	public void stuff() throws IOException {
		final StreamConnection connection = notifier.acceptAndOpen();
		System.out.println("Device connected");
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					DataInputStream in = connection.openDataInputStream();
					Nugget<?> nugget = Nugget.readNugget(in);
					in.close();
					connection.close();
					System.out.println("Device disconnected");
					NuggetSerializable<LinkedList<HashMap<String, HashMap<String, Object>>>> cast =
							(NuggetSerializable<LinkedList<HashMap<String, HashMap<String, Object>>>>) nugget;
					saveStuff(cast.getValue());

				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();

	}

	private void saveStuff(LinkedList<HashMap<String, HashMap<String, Object>>> data_list) {
		try {
			Connection connection = ScouterClient.getDatabaseHandler().getDatabaseConnection();
			for (HashMap<String, HashMap<String, Object>> data : data_list) {
				saveMatch(connection, data.get("teleop"), true);
				saveMatch(connection, data.get("autonomous"), false);
			}
			connection.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void saveMatch(Connection connection, HashMap<String, Object> bundle, boolean teleop) throws SQLException {
		if (bundle == null || bundle.isEmpty()) {
			return;
		}
		// Delete already-existing rows for this match/team/mode combination
		new SQLQuery(connection, "DELETE FROM `scouting` WHERE `match_number`=? AND `team_number`=? AND `teleop`=?")
				.setInt((int) bundle.get("match_number"), 1)
				.setInt((int) bundle.get("team_number"), 2)
				.setBoolean(teleop, 3)
				.execute();
		// Begin building the INSERT statement
		StringBuilder query_builder = new StringBuilder("INSERT INTO `scouting` (");
		LinkedList<Object> bindargs = new LinkedList<>();
		// Add an argument for each key/value pair of the bundle
		for (String key : bundle.keySet()) {
			query_builder.append("`").append(key).append("`, ");
			bindargs.add(bundle.get(key));
		}
		// Get rid of that pesky trailing comma
		query_builder.delete(query_builder.length() - 2, query_builder.length());
		query_builder.append(") VALUES (");
		Object[] bindargs_array = bindargs.toArray();
		for (int i = 0; i < bindargs_array.length; i++) {
			query_builder.append("?");
			if (i < bindargs_array.length - 1) {
				query_builder.append(", ");
			}
		}
		query_builder.append(")");
		// Execute the statement
		new SQLQuery(connection, query_builder.toString())
				.bindAll(bindargs_array)
				.execute();
	}
}
