package com.shortcircuit.scouterclient.ui;

import com.shortcircuit.scouterclient.comm.BluetoothStuff;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

/**
 * @author ShortCircuit908
 *         Created on 1/14/2016
 */
public class ConnectionForm {
	private JPanel content_panel;
	private JToggleButton toggle_accept_connections;
	private BluetoothStuff stuff;

	public ConnectionForm() {
		JFrame frame = new JFrame("Scouter Windows Client");
		frame.setContentPane(content_panel);
		frame.setLocationByPlatform(true);
		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				destroyStuff();
				System.exit(0);
			}
		});
		toggle_accept_connections.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (toggle_accept_connections.isSelected()) {
					createStuff();
				}
				else {
					destroyStuff();
				}
			}
		});
		frame.pack();
		frame.setVisible(true);
	}

	private void createStuff() {
		if (stuff != null) {
			return;
		}
		try {
			stuff = new BluetoothStuff();
			System.out.println("Now listening for connections");
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void destroyStuff() {
		if (stuff == null) {
			return;
		}
		try {
			stuff.close();
			stuff = null;
			System.out.println("No longer listening for connections");
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
