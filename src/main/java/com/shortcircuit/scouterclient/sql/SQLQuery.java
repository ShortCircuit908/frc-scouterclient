package com.shortcircuit.scouterclient.sql;


import java.math.BigDecimal;
import java.sql.*;
import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;

/**
 * @author ShortCircuit908
 */
@SuppressWarnings("unused")
public class SQLQuery {
	private Connection connection;
	private PreparedStatement statement;
	private ResultSet result;
	private int row = 1;
	private boolean auto_close = true;
	private static final boolean DEBUG_FETCHALL = false;

	/**
	 * Creates a new SQLQuery with the provided SQL statement
	 * <p/>
	 * This constructor produces a query which does not close its connection automatically
	 *
	 * @param connection A connection to a database
	 * @param raw_query  Any valid SQL statement
	 * @throws SQLException if something bad happens
	 */
	public SQLQuery(Connection connection, String raw_query) throws SQLException {
		this.connection = connection;
		auto_close = false;
		statement = connection.prepareStatement(raw_query);
	}

	/**
	 * Creates a new SQLQuery with the provided SQL statement
	 * <p/>
	 * This constructor produces a query which does not close its connection automatically
	 *
	 * @param db_handler The database handler to use
	 * @param raw_query  Any valid SQL statement
	 * @throws SQLException if something bad happens
	 */
	public SQLQuery(DatabaseHandler db_handler, String raw_query) throws SQLException {
		try {
			connection = db_handler.getDatabaseConnection();
			if (connection == null) {
				throw new SQLException("Could not establish connection with database");
			}
			statement = connection.prepareStatement(raw_query);
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			closeConnection(0, false);
			throw e;
		}
	}

	/**
	 * Sets the value of any parameters in the SQL query at the given column indices
	 *
	 * @param value          The value to be set
	 * @param column_indices The indices of any columns whose values are to be set
	 * @return this
	 * @throws SQLException if something bad happens
	 */
	public SQLQuery setInt(Integer value, int... column_indices) throws SQLException {
		try {
			for (int column_index : column_indices) {
				statement.setInt(column_index, value);
			}
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Sets the value of any parameters in the SQL query at the given column indices
	 *
	 * @param value          The value to be set
	 * @param column_indices The indices of any columns whose values are to be set
	 * @return this
	 * @throws SQLException if something bad happens
	 */
	public SQLQuery setBoolean(Boolean value, int... column_indices) throws SQLException {
		try {
			for (int column_index : column_indices) {
				statement.setBoolean(column_index, value);
			}
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Sets the value of any parameters in the SQL query at the given column indices
	 *
	 * @param value          The value to be set
	 * @param column_indices The indices of any columns whose values are to be set
	 * @return this
	 * @throws SQLException if something bad happens
	 */
	public SQLQuery setArray(Array value, int... column_indices) throws SQLException {
		try {
			for (int column_index : column_indices) {
				statement.setArray(column_index, value);
			}
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Sets the value of any parameters in the SQL query at the given column indices
	 *
	 * @param value          The value to be set
	 * @param column_indices The indices of any columns whose values are to be set
	 * @return this
	 * @throws SQLException if something bad happens
	 */
	public SQLQuery setDouble(Double value, int... column_indices) throws SQLException {
		try {
			for (int column_index : column_indices) {
				statement.setDouble(column_index, value);
			}
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Sets the value of any parameters in the SQL query at the given column indices
	 *
	 * @param value          The value to be set
	 * @param column_indices The indices of any columns whose values are to be set
	 * @return this
	 * @throws SQLException if something bad happens
	 */
	public SQLQuery setFloat(Float value, int... column_indices) throws SQLException {
		try {
			for (int column_index : column_indices) {
				statement.setFloat(column_index, value);
			}
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Sets the value of any parameters in the SQL query at the given column indices
	 *
	 * @param value          The value to be set
	 * @param column_indices The indices of any columns whose values are to be set
	 * @return this
	 * @throws SQLException if something bad happens
	 */
	public SQLQuery setLong(Long value, int... column_indices) throws SQLException {
		try {
			for (int column_index : column_indices) {
				statement.setLong(column_index, value);
			}
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Sets the value of any parameters in the SQL query at the given column indices
	 *
	 * @param value          The value to be set
	 * @param column_indices The indices of any columns whose values are to be set
	 * @return this
	 * @throws SQLException if something bad happens
	 */
	public SQLQuery setBlob(Blob value, int... column_indices) throws SQLException {
		try {
			for (int column_index : column_indices) {
				statement.setBlob(column_index, value);
			}
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Sets the value of any parameters in the SQL query at the given column indices
	 *
	 * @param value          The value to be set
	 * @param column_indices The indices of any columns whose values are to be set
	 * @return this
	 * @throws SQLException if something bad happens
	 */
	public SQLQuery setClob(Clob value, int... column_indices) throws SQLException {
		try {
			for (int column_index : column_indices) {
				statement.setClob(column_index, value);
			}
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Sets the value of any parameters in the SQL query at the given column indices
	 *
	 * @param value          The value to be set
	 * @param column_indices The indices of any columns whose values are to be set
	 * @return this
	 * @throws SQLException if something bad happens
	 */
	public SQLQuery setByte(Byte value, int... column_indices) throws SQLException {
		try {
			for (int column_index : column_indices) {
				statement.setByte(column_index, value);
			}
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Sets the value of any parameters in the SQL query at the given column indices
	 *
	 * @param value          The value to be set
	 * @param column_indices The indices of any columns whose values are to be set
	 * @return this
	 * @throws SQLException if something bad happens
	 */
	public SQLQuery setBytes(byte[] value, int... column_indices) throws SQLException {
		try {
			for (int column_index : column_indices) {
				statement.setBytes(column_index, value);
			}
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Sets the value of any parameters in the SQL query at the given column indices
	 *
	 * @param value          The value to be set
	 * @param column_indices The indices of any columns whose values are to be set
	 * @return this
	 * @throws SQLException if something bad happens
	 */
	public SQLQuery setString(String value, int... column_indices) throws SQLException {
		try {
			for (int column_index : column_indices) {
				statement.setString(column_index, value);
			}
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Sets the value of any parameters in the SQL query at the given column indices
	 *
	 * @param value          The value to be set
	 * @param column_indices The indices of any columns whose values are to be set
	 * @return this
	 * @throws SQLException if something bad happens
	 */
	public SQLQuery setObject(Object value, int... column_indices) throws SQLException {
		try {
			for (int column_index : column_indices) {
				statement.setObject(column_index, value);
			}
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	public SQLQuery bindAll(Iterable<Object> values) throws SQLException {
		return bindAll(1, values);
	}

	public SQLQuery bindAll(int start_index, Iterable<Object> values) throws SQLException {
		try {
			int i = start_index;
			for (Object obj : values) {
				statement.setObject(i++, obj);
			}
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	public SQLQuery bindAll(Object... values) throws SQLException {
		return bindAll(1, values);
	}


	public SQLQuery bindAll(int start_index, Object... values) throws SQLException {
		try {
			for (int i = 0; i < values.length; i++) {
				statement.setObject(i + start_index, values[i]);
			}
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Sets the value of any parameters in the SQL query at the given column indices
	 *
	 * @param value          The value to be set
	 * @param column_indices The indices of any columns whose values are to be set
	 * @return this
	 * @throws SQLException if something bad happens
	 */
	public SQLQuery setDate(Date value, int... column_indices) throws SQLException {
		try {
			for (int column_index : column_indices) {
				statement.setDate(column_index, value);
			}
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Sets the value of any parameters in the SQL query at the given column indices
	 *
	 * @param value          The value to be set
	 * @param column_indices The indices of any columns whose values are to be set
	 * @return this
	 * @throws SQLException if something bad happens
	 */
	public SQLQuery setBigDecimal(BigDecimal value, int... column_indices) throws SQLException {
		try {
			for (int column_index : column_indices) {
				statement.setBigDecimal(column_index, value);
			}
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Execute the query
	 *
	 * @return this
	 * @throws SQLException if something bad happens
	 * @see PreparedStatement#executeQuery()
	 */
	public SQLQuery executeQuery() throws SQLException {
		final long duration = System.nanoTime();
		try {
			result = statement.executeQuery();
			displayStatus(duration, true, -1);
			if (auto_close) {
				closeConnection(duration, true);
			}
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Execute the query
	 *
	 * @return Success
	 * @throws SQLException if something bad happens
	 * @see PreparedStatement#execute()
	 */
	public boolean execute() throws SQLException {
		final long duration = System.nanoTime();
		try {
			boolean result = statement.execute();
			if (result) {
				this.result = statement.getResultSet();
			}
			displayStatus(duration, true, -1);
			if (auto_close) {
				closeConnection(duration, true);
			}
			return result;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Executes the query
	 *
	 * @return Thing
	 * @throws SQLException if something bad happens
	 * @see PreparedStatement#executeUpdate()
	 */
	public int update() throws SQLException {
		final long duration = System.nanoTime();
		try {
			int result = statement.executeUpdate();
			displayStatus(duration, true, -1);
			if (auto_close) {
				closeConnection(duration, true);
			}
			return result;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Determines whether a given row exists within the {@link ResultSet ResultSet}
	 *
	 * @param row Index of the row to check
	 * @return <code>true</code> if the row exists, <code>false</code> otherwise
	 * @throws SQLException if something bad happens
	 */
	public boolean rowExists(int row) throws SQLException {
		try {
			boolean exists = result.absolute(row);
			result.absolute(this.row);
			return exists;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Moves the {@link ResultSet ResultSet} to the next row
	 *
	 * @return <code>true</code> if the row exists, <code>false</code> otherwise
	 * @throws SQLException if something bad happens
	 */
	public boolean nextRow() throws SQLException {
		try {
			boolean next = result.next();
			if (next) {
				row++;
			}
			return next;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Sets the row position of the {@link ResultSet ResultSet}
	 *
	 * @param row The row index
	 * @return this
	 * @throws SQLException if something bad happens
	 * @see ResultSet#absolute(int)
	 */
	public SQLQuery setRow(int row) throws SQLException {
		try {
			result.absolute(row);
			this.row = row;
			return this;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Fetches a single value from a given column at the current row
	 *
	 * @param column_index The desired column index
	 * @param clazz        The Java class types of the corresponding column
	 * @param <T>          The desired Java type
	 * @return The value of the column
	 * @throws SQLException if something bad happens
	 */
	public <T> T fetch(int column_index, Class<T> clazz) throws SQLException, ClassCastException {
		try {
			if (rowExists(row)) {
				return ClassUtils.cast(result.getObject(column_index), clazz);
			}
			return null;
		}
		catch (ClassCastException e) {
			throw e;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Fetch an entire column and store it in the provided {@link Column Column}
	 *
	 * @param column_index The desired column index
	 * @param column       The column to populate
	 * @return this
	 * @throws SQLException
	 */
	public <T> SQLQuery fetchColumn(int column_index, Column<T> column) throws SQLException, ClassCastException {
		try {
			int row = result.getRow();
			result.beforeFirst();
			while (nextRow()) {
				T obj = ClassUtils.cast(result.getObject(column_index), column.getContentClass());
				column.addElement(obj);
			}
			result.absolute(row);
			return this;
		}
		catch (ClassCastException e) {
			throw e;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	/**
	 * Fetch the contents of multiple columns and store them in the provided {@link Column Column}s
	 * <p>The index of each provided parameter corresponds to the column index of the {@link ResultSet ResultSet}
	 * <p>Passing either <code>null</code> or <code>Column<Void></code> as a parameter indicates that the column should be skipped entirely
	 *
	 * @param columns The columns to populate
	 * @return this
	 * @throws SQLException
	 */
	public SQLQuery fetchAll(Column<?>... columns) throws SQLException, ClassCastException {
		try {
			for (int i = 0; i < columns.length; i++) {
				boolean skip = columns[i] == null || columns[i].isVoid();
				if (skip) {
					continue;
				}
				fetchColumn(i + 1, columns[i]);
			}
			return this;
		}
		catch (ClassCastException e) {
			throw e;
		}
		catch (Throwable e) {
			displayStatus(0, false, -1);
			if (auto_close) {
				closeConnection(0, false);
			}
			throw e;
		}
	}

	private void displayStatus(long duration, boolean success, int rows_changed) {
		StackTraceElement caller_trace = ClassUtils.getCallerTrace(rows_changed == -1 ? 4 : 3);
		System.out.println("Query from " + caller_trace.getClassName() + "." + caller_trace.getMethodName()
				+ "(" + caller_trace.getFileName() + ":" + caller_trace.getLineNumber()
				+ ") " + (success ? "complete" : "failed") + " - "
				+ debugRunDuration(duration, TimeUnit.NANOSECONDS) + "s elapsed"
				+ (rows_changed > -1 ? ", " + rows_changed + " row(s) changed" : ""));
	}

	/**
	 * Closes the connection to the SQL database
	 *
	 * @param duration     The time, in nanoseconds, just before the query was executed
	 * @param success      If {@code true}, prints debug info to the logger
	 * @param rows_changed The number of rows changed, -1 if not applicable
	 * @throws SQLException if something bad happens
	 */
	private void closeConnection(long duration, boolean success, int rows_changed) throws SQLException {
		if (connection != null && !connection.isClosed()) {
			connection.close();
		}
		if (statement != null && !statement.isClosed()) {
			statement.close();
		}
	}

	/**
	 * Closes the connection to the SQL database
	 *
	 * @param duration The time, in nanoseconds, just before the query was executed
	 * @param success  If {@code true}, prints debug info to the logger
	 * @throws SQLException if something bad happens
	 */
	private void closeConnection(long duration, boolean success) throws SQLException {
		closeConnection(duration, success, -1);
	}

	public static String debugRunDuration(long duration, TimeUnit units) {
		long duration_nanos = TimeUnit.NANOSECONDS.convert(duration, units);
		double elapsed = System.nanoTime() - duration_nanos;
		elapsed /= 1000000000.0;
		double rounded = Math.round(elapsed * 10000.0) / 10000.0;
		return new DecimalFormat("0.0000").format(rounded);
	}
}
