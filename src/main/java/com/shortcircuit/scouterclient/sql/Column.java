package com.shortcircuit.scouterclient.sql;

import java.util.*;

/**
 * Represents a column of data in an SQL table as an immutable list
 *
 * @author ShortCircuit908
 *         Created on 10/29/2015
 */
@SuppressWarnings("NullableProblems")
public final class Column<T> implements List<T>, RandomAccess {
	private final ArrayList<T> elements = new ArrayList<>();
	private final Class<T> content_class;

	public Column(Class<T> content_class) {
		this.content_class = content_class;
	}

	protected void addElement(T element) {
		elements.add(element);
	}

	public Class<T> getContentClass() {
		return content_class;
	}

	public boolean isVoid() {
		return content_class == null || content_class.equals(Void.class) || content_class.equals(void.class);
	}

	@Override
	public int size() {
		return elements.size();
	}

	@Override
	public boolean isEmpty() {
		return elements.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return elements.contains(o);
	}

	@Override
	public Iterator<T> iterator() {
		return new ImmutableListIterator(elements.listIterator());
	}

	@Override
	public Object[] toArray() {
		return elements.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return elements.toArray(a);
	}

	@Override
	public boolean add(T t) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return elements.containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException();
	}

	@Override
	public T get(int index) {
		return elements.get(index);
	}

	@Override
	public T set(int index, T element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void add(int index, T element) {
		throw new UnsupportedOperationException();
	}

	@Override
	public T remove(int index) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int indexOf(Object o) {
		return elements.indexOf(o);
	}

	@Override
	public int lastIndexOf(Object o) {
		return elements.lastIndexOf(o);
	}

	@Override
	public ListIterator<T> listIterator() {
		return new ImmutableListIterator(elements.listIterator());
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		return new ImmutableListIterator(elements.listIterator(index));
	}

	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		return elements.subList(fromIndex, toIndex);
	}

	private class ImmutableListIterator implements ListIterator<T> {
		private final ListIterator<T> iterator;

		private ImmutableListIterator(ListIterator<T> iterator) {
			this.iterator = iterator;
		}

		@Override
		public boolean hasNext() {
			return iterator.hasNext();
		}

		@Override
		public T next() {
			return iterator.next();
		}

		@Override
		public boolean hasPrevious() {
			return iterator.hasPrevious();
		}

		@Override
		public T previous() {
			return iterator.previous();
		}

		@Override
		public int nextIndex() {
			return iterator.nextIndex();
		}

		@Override
		public int previousIndex() {
			return iterator.previousIndex();
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

		@Override
		public void set(T t) {
			throw new UnsupportedOperationException();
		}

		@Override
		public void add(T t) {
			throw new UnsupportedOperationException();
		}
	}
}
