package com.shortcircuit.scouterclient.sql;

import org.sqlite.SQLiteConfig;
import org.sqlite.javax.SQLiteConnectionPoolDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author ShortCircuit908
 */
public class SQLiteDBHandler extends DatabaseHandler {
	private final SQLiteConnectionPoolDataSource connection_pool;

	/**
	 * Create a new SQLite database handler using the given configuration
	 *
	 * @param properties   The configuration for the data source.
	 * @param database_url The location of the database file.
	 */
	public SQLiteDBHandler(Properties properties, String database_url) {
		this(new SQLiteConfig(properties), database_url);
	}

	/**
	 * Create a new SQLite database handler using the given configuration
	 *
	 * @param config       The configuration for the data source.
	 * @param database_url The location of the database file.
	 */
	public SQLiteDBHandler(SQLiteConfig config, String database_url) {
		database_url = "jdbc:sqlite:" + database_url;
		connection_pool = new SQLiteConnectionPoolDataSource(config);
		connection_pool.setUrl(database_url);
	}

	/**
	 * Create a new SQLite database handler with default configuration values
	 *
	 * @param database_url The location of the database file.
	 */
	public SQLiteDBHandler(String database_url) {
		database_url = "jdbc:sqlite:" + database_url;
		SQLiteConfig config = new SQLiteConfig();
		config.setBusyTimeout("3000");
		connection_pool = new SQLiteConnectionPoolDataSource(config);
		connection_pool.setUrl(database_url);
	}

	/**
	 * {@inheritDoc}
	 */
	public Connection getDatabaseConnection() throws SQLException {
		return connection_pool.getPooledConnection().getConnection();
	}
}
