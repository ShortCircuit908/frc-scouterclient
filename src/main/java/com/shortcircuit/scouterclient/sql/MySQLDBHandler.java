package com.shortcircuit.scouterclient.sql;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author ShortCircuit908
 */
public class MySQLDBHandler extends DatabaseHandler {
	private final HikariDataSource connection_pool;

	/**
	 * Create a new MySQL database handler using the given configuration
	 *
	 * @param properties_file_name Name of the properties file to use
	 */
	public MySQLDBHandler(String properties_file_name) {
		this(new HikariConfig(properties_file_name));
	}

	/**
	 * Create a new MySQL database handler using the given configuration
	 *
	 * @param properties The configuration for the data source.
	 */
	public MySQLDBHandler(Properties properties) {
		this(new HikariConfig(properties));
	}

	/**
	 * Create a new MySQL database handler using the given configuration
	 *
	 * @param config The configuration for the data source.
	 */
	public MySQLDBHandler(HikariConfig config) {
		connection_pool = new HikariDataSource(config);
	}

	/**
	 * Create a new MySQL database handler using default configuration values
	 *
	 * @param database_username Username
	 * @param database_password Password
	 * @param database_host     Database host URL
	 * @param database_port     Database host port
	 * @param db                Name of the database
	 */
	public MySQLDBHandler(String database_username, String database_password, String database_host, int database_port, String db) {
		String database_url = "jdbc:mysql://" + database_host + ":" + database_port + "/" + db;
		HikariConfig config = new HikariConfig();
		config.setMaximumPoolSize(8);
		config.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
		config.addDataSourceProperty("url", database_url);
		config.addDataSourceProperty("user", database_username);
		config.addDataSourceProperty("password", database_password);
		config.setConnectionTimeout(3000);
		config.addDataSourceProperty("cachePrepStmts", true);
		config.addDataSourceProperty("prepStmtCacheSize", 250);
		config.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
		config.addDataSourceProperty("useServerPrepStmts", true);
		connection_pool = new HikariDataSource(config);
	}

	/**
	 * {@inheritDoc}
	 */
	public Connection getDatabaseConnection() throws SQLException {
		return connection_pool.getConnection();
	}
}
