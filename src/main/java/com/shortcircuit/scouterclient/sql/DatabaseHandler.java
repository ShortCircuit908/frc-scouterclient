package com.shortcircuit.scouterclient.sql;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * @author ShortCircuit908
 */
public abstract class DatabaseHandler {
	/**
	 * Get a new connection to the database for use with an {@link com.shortcircuit.utils.sql.SQLQuery SQLQuery}
	 *
	 * @return The new database connection
	 * @throws SQLException
	 */
	public abstract Connection getDatabaseConnection() throws SQLException;

	/**
	 * Execute an SQL schema file
	 *
	 * @param schema_file The schema file to execute
	 * @throws SQLException
	 */
	public void createFromSchema(File schema_file) throws SQLException {
		try {
			Scanner scanner = new Scanner(schema_file);
			scanner.useDelimiter(";");
			List<String> raw_queries = new LinkedList<>();
			while (scanner.hasNext()) {
				String raw_query = scanner.next();
				if (!raw_query.trim().isEmpty()) {
					raw_queries.add(raw_query);
				}
			}
			long duration = System.nanoTime();
			for (String raw_query : raw_queries) {
				new SQLQuery(this, raw_query).execute();
			}
			System.out.println("Database populated from " + schema_file.getName() + " - "
					+ SQLQuery.debugRunDuration(duration, TimeUnit.NANOSECONDS));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
