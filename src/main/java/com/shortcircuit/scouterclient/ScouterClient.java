package com.shortcircuit.scouterclient;

import com.shortcircuit.scouterclient.sql.MySQLDBHandler;
import com.shortcircuit.scouterclient.ui.ConnectionForm;

import java.io.IOException;
import java.sql.SQLException;

/**
 * @author ShortCircuit908
 *         Created on 1/14/2016
 */
public class ScouterClient {
	private static final MySQLDBHandler db_handler =
			new MySQLDBHandler("root", "PIrAtEs", "localhost", 3306, "StrongholdScouting");

	public static void main(String... args) throws ClassNotFoundException, IOException, SQLException {
		new ConnectionForm();
	}

	public static MySQLDBHandler getDatabaseHandler() {
		return db_handler;
	}
}
